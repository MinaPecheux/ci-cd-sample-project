from flask import Flask, render_template, request

import functions

VERSION = '1.0.1'
app = Flask(__name__)

@app.route('/')
def index():
  return render_template('index.html', data={'version': VERSION})

@app.route('/add', methods=['POST'])
def add():
  if request.method == 'POST':
    input = request.form
    result = functions.add(
      int(input['input-1']),
      int(input['input-2']))
  return render_template('submit.html', data={
    **input,
    'version': VERSION,
    'operation': '+',
    'result': result})

@app.route('/subtract', methods=['POST'])
def subtract():
  if request.method == 'POST':
    input = request.form
    result = functions.subtract(
      int(input['input-1']),
      int(input['input-2']))
  return render_template('submit.html', data={
    **input,
    'version': VERSION,
    'operation': '-',
    'result': result})

@app.route('/multiply', methods=['POST'])
def multiply():
  if request.method == 'POST':
    input = request.form
    result = functions.multiply(
      int(input['input-1']),
      int(input['input-2']))
  return render_template('submit.html', data={
    **input,
    'version': VERSION,
    'operation': '*',
    'result': result})

@app.route('/divide', methods=['POST'])
def divide():
  if request.method == 'POST':
    input = request.form
    result = functions.divide(
      int(input['input-1']),
      int(input['input-2']))
  return render_template('submit.html', data={
    **input,
    'version': VERSION,
    'operation': '/',
    'result': result})

if __name__ == '__main__':
  app.run(debug=True)
